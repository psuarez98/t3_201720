package model.data_structures.test;

import junit.framework.TestCase;
import model.data_structures.*;

public class StackTest extends TestCase{
	
	//------------------------------------------------------
	//Atributo
	//------------------------------------------------------
	
	private Stack<Integer> stack;
	
	//------------------------------------------------------
	//setUp
	//------------------------------------------------------
	
	/**
	 * Se inicializa la pila vac�a
	 */
	public void setUp(){
		stack = new Stack<Integer>();
	}
	
	/**
	 * La pila va a tener un �nico elemento
	 */	
	public void setUpEscenario1(){
		setUp();
		stack.push(5);
	}
	
	//------------------------------------------------------
	//Tests
	//------------------------------------------------------
	
	/**
	 * Caso 1: La lista est� vac�a
	 * Caso 2: La lista tiene un solo elemento
	 */
	public void testGetSize(){
		setUp();
		assertEquals("Debi� retornar 0", 0, stack.getSize());
		
		setUpEscenario1();
		assertEquals("Debi� retornar 1", 1, stack.getSize());
	}
	
	/**
	 * Caso 1: La lista est� vac�a
	 * Caso 2: La lista tiene un solo elemento
	 */
	public void testGetTop(){
		setUp();
		assertNull("Deber�a ser null", stack.getTop());
		setUpEscenario1();
		assertEquals("Debi� retornar 5", Integer.valueOf(5), stack.getTop());
	}
	
	/**
	 * Caso 1: La lista est� vac�a
	 * Caso 2: La lista tiene un solo elemento
	 */
	public void testPop(){
		setUp();
		assertNull("Deber�a retornar null", stack.pop());
		
		setUpEscenario1();
		assertEquals("Deber�a ser 5", Integer.valueOf(5), stack.pop());
	}
	
	/**
	 * Caso 1: La lista est� vac�a
	 * Caso 2: La lista tiene un solo elemento
	 */
	public void testPush(){
		setUp();
		stack.push(10);
		assertEquals("Debi� retornar 10", Integer.valueOf(10), stack.getTop());
		
		setUpEscenario1();
		assertEquals("Debi� retornar 5", Integer.valueOf(5), stack.getTop());
	}
}
