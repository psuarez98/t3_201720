package model.data_structures.test;

import junit.framework.TestCase;
import model.data_structures.*;

public class QueueTest extends TestCase{

	//--------------------------------------
	//Atributo
	//--------------------------------------
	private Queue<Integer> queue;
	
	//--------------------------------------
	//setUp
	//--------------------------------------
	
	/**
	 * Inicializa la cola vac�a
	 */
	public void setUp(){
		queue = new Queue<Integer>();
	}
	
	/**
	 * Le agrega un solo elemento a la cola
	 */
	public void setUpEscenario1(){
		queue.enqueue(5);
	}
	
	//--------------------------------------
	//Tests
	//--------------------------------------
	
	public void testEnqueue(){
		setUp();
		setUpEscenario1();
		assertEquals("Deber�a retornar 5", Integer.valueOf(5), queue.dequeue());
	}
	
	public void testDequeue(){
		setUp();
		setUpEscenario1();
		
		queue.dequeue();
		assertEquals("Deber�a ser 0", 0, queue.getSize());
	}
	
	public void testIsEmpty(){
		setUp();
		assertTrue("Deber�a ser true", queue.isEmpty());
		
		setUpEscenario1();
		assertFalse("Deber�a ser false", queue.isEmpty());
	}
	
	public void testYaExiste(){
		setUp();
		setUpEscenario1();
		
		assertTrue("Deber�a ser true",  queue.yaExiste(Integer.valueOf(5)));
	}
}
