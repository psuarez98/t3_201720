package model.data_structures;

public class Queue<T> implements IQueue<T>{

	private NodeQueue<T>primero;
	private int tamanio;


	/**
	 * M�todo constructor
	 */
	public Queue()
	{
		primero=null;
		tamanio=0;
	}

	/**
	 * M�todo que verifica si est� vac�o
	 * @return true si lo es, false de lo contrario
	 */
	public boolean isEmpty()
	{
		if(primero==null)
		{
			return true;
		}
		return false;
	}
	@Override
	public void enqueue(T item) {
		// TODO Auto-generated method stub
		NodeQueue<T> agregar = new NodeQueue<T>(item, String.valueOf(item));
		if(tamanio == 0){
			primero = agregar;
		}
		else if(yaExiste(item)){
			System.out.println("Ya existe un elemento con ese identificador");
			return;
		}
		else if(tamanio == 0){
			primero=agregar;
		}
		else{
			NodeQueue<T> actual = primero;
			for(int i=1; i<tamanio; i++){
				if(actual.darSig() == null){
					actual.cambiarSig(agregar);
					agregar.cambiarSig(null);
					break;
				}
				else
					actual = actual.darSig();
			}
		}
		tamanio ++;


	}
	public boolean yaExiste(T item)
	{
		boolean existe = false;
		if(tamanio == 0)

			return false;
		else{

			if(primero.darElemento().equals(item))
				return true;

			else{
				NodeQueue<T> nActual = primero;
				for(int i=0; i<tamanio && !existe; i++){
					if(nActual.darElemento().equals(item))
						existe = true;
				}
			}
			return existe;
		}

	}

	@Override
	public T dequeue() {
		// TODO Auto-generated method stub

		if(primero == null)
			return null;
		else{
			
			NodeQueue<T> nuevoPrimero = primero.darSig();
			NodeQueue<T> elementoARetornar = primero;
			primero = nuevoPrimero;
			tamanio --;
			return elementoARetornar.darElemento();
		}
	}
	
	public int getSize(){
		return tamanio;
	}

}
