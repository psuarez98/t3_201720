package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.google.gson.reflect.TypeToken;

import model.data_structures.IStack;
import model.data_structures.Queue;
import model.data_structures.RingList;
import model.data_structures.Stack;
import model.vo.BusUpdateVO;
import model.vo.StopVO;


import api.ISTSManager;

public class STSManager implements ISTSManager{
	
	private RingList<StopVO> stops;
	private Queue<BusUpdateVO> updates;
	

	public void readBusUpdate(File rtFile) throws Exception 
	{
		Gson gson = new Gson(); 
		
		updates = new Queue<BusUpdateVO>();
		
		BufferedReader br = new BufferedReader(new FileReader(rtFile));
		JsonParser parser = new JsonParser();
		JsonArray array = parser.parse(br).getAsJsonArray();
		
		for(int i = 0 ; i < array.size() ; i++) 
		{
			JsonElement yourJson = array.get(i);
			String result1 = yourJson.toString();
			JsonObject objeto = yourJson.getAsJsonObject();
			BusUpdateVO newUpdates = gson.fromJson(objeto.toString(), BusUpdateVO.class);
			updates.enqueue(newUpdates);
		}			
	}		

	/**
	 * 1. Buscar el bus al que corresponde el tripID que entra por par�metro
	 * 2. Ver por cu�les pares ordenados (lat, lon) pasa
	 * 3. Comparar cada par ordenado con todas las posibles paradas y ver si la distancia es menor a 70 metros
	 * 4. Agregar a una Stack las paradas que cumplan la condici�n de que el bus pase a menos de 70 metros de distancia
	 * @return IStack<StopVO> Una pila con las paradas
	 */
	@Override
	public IStack <StopVO> listStops(Integer tripID)
	{
		Stack<StopVO> laPila= new Stack<StopVO>();
		
		BusUpdateVO act=null;
		while(!updates.isEmpty())
		{
			act=updates.dequeue();
			if(act.getTripId()==tripID)
			{
				Iterator <StopVO> iterator=stops.iterator();
				while(iterator.hasNext())
				{
					StopVO paradaActual= iterator.next();
					double distance = getDistance(act.getLatitude(), act.getLongitude(), paradaActual.getStop_lat(),paradaActual.getStop_lon());
					if(distance<=70)
					{
						laPila.push(paradaActual);
					}
				}
				}
				
		}
		return laPila;
	}

	public double getDistance(double lat1, double lon1, double lat2, double lon2) {
        // TODO Auto-generated method stub
        final int R = 6371*1000; // Radious of the earth
    
        double latDistance = toRad(lat2-lat1);
        double lonDistance = toRad(lon2-lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + 
                   Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) * 
                   Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double distance = R * c;
         
        return distance;
 
    }

	private Double toRad(Double value) {
        return value * Math.PI / 180;
    }

	
	public void loadStops(String stopsFile) {
		// TODO Auto-generated method stub
		stops = new RingList<StopVO>();
		FileReader lector;
		BufferedReader in;
		String linea = null ;
		try {

			lector = new FileReader(stopsFile);
			in = new BufferedReader(lector);
			linea = in.readLine();

			while(linea != null)
			{

				String[] datos = linea.split(",");
				int trip_id =Integer.parseInt( datos[0]);
				int stop_code= Integer.parseInt( datos[1]);
				String stopName= ( datos[3]);
				String stop_desc []= datos[4].split("@");
				String calle= stop_desc[0];
				String avenida= stop_desc[1];
				double stop_lat= Double.parseDouble(datos[5]);
				double stop_lon= Double.parseDouble(datos[6]);
				String zone_id []= datos[7].split(" ");
				String letra= zone_id[0];
				int num= Integer.parseInt(zone_id[1]);
				String stop_url = datos[8];
				
				String location_type= datos[9];
				int parent_station= Integer.parseInt(datos[10]);
					
				StopVO nueva= new StopVO(trip_id, stop_code, stopName, calle, avenida, stop_lat, stop_lon, letra,num,stop_url, location_type, parent_station);
				
				try 
				{
					stops.add(nueva);
				} 
				catch (Exception e) 
				{
					// Se ignora y se continua con el sigueinte
				}
				linea = in.readLine();
			}
			in.close();
			lector.close();
		}
		catch (Exception e) 
		{
			
		}

	}

	

	

}
